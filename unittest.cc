
/**
 * @file unittest.cc
 * @author Uwe Koecher (UK)
 * @date 2018-06-20, UK
 *
 * @brief unit test: make_hanging_node_constraints fails
 * - parallel / distributed
 * - complicated mesh
 * - FESystem with 2 FE's having hanging nodes
 * 
 * NOTE this fails: mpirun -np 9 ./unittest
 */

#define MPIX_THREADS 1

#include <deal.II/base/utilities.h>
#include <deal.II/base/index_set.h>

#include <deal.II/base/quadrature_lib.h>

#include <deal.II/distributed/tria.h>
#include <deal.II/distributed/grid_refinement.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/fe/fe_raviart_thomas.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_system.h>

#include <deal.II/lac/constraint_matrix.h>

using namespace dealii;

#define dim 2

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	const unsigned int MyPID(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD));
	
	try {
		auto mpi_comm{MPI_COMM_WORLD};
		
		auto tria = std::make_shared<parallel::distributed::Triangulation<dim>>(mpi_comm);
		
		////////////////////////////////////////////////////////////////////////
		//  Grid In: read Dyke.msh on all procs
		{
			dealii::GridIn<dim> grid_in;
			grid_in.attach_triangulation(*tria);
			AssertThrow(dim==2, dealii::ExcNotImplemented());
			std::ifstream input_msh("Dyke.msh", std::ios_base::in);
			grid_in.read_msh(input_msh);
			input_msh.close();
		}
		// sync all processes
		MPI_Barrier(mpi_comm);
		
		////////////////////////////////////////////////////////////////////////
		// local refinement loop 1
		{
			auto cell = tria->begin_active();
			auto endc = tria->end();
			
			for ( ; cell != endc; ++cell ) {
				const auto center{cell->center()};
				
				if ( ((center[0] >= -7) && (center[0] <= -5) ) && (center[1] >= -.5) ) {
					cell->set_refine_flag(
						dealii::RefinementCase< dim >::isotropic_refinement
					);
				}
				else if ( ((center[0] >= 2.5) && (center[0] <= 3.5) ) && (center[1] >= -.5) ) {
					cell->set_refine_flag(
						dealii::RefinementCase< dim >::isotropic_refinement
					);
				}
			}
		}
		
		tria->execute_coarsening_and_refinement();
		
		////////////////////////////////////////////////////////////////////////
		// local refinement loop 2
		{
			auto cell = tria->begin_active();
			auto endc = tria->end();
			
			for ( ; cell != endc; ++cell ) {
				const auto center{cell->center()};
				
				if ( ((center[0] >= -6.5) && (center[0] <= -5.5) ) && (center[1] >= -.25) ) {
					cell->set_refine_flag(
						dealii::RefinementCase< dim >::isotropic_refinement
					);
				}
				else if ( ((center[0] >= 2.75) && (center[0] <= 3.25) ) && (center[1] >= -.25) ) {
					cell->set_refine_flag(
						dealii::RefinementCase< dim >::isotropic_refinement
					);
				}
			}
		}
		
		tria->execute_coarsening_and_refinement();
		
		////////////////////////////////////////////////////////////////////////
		
		DoFHandler<dim> dof(*tria);
		
		const unsigned int p{1};
		auto fe = std::make_shared< dealii::FESystem<dim> > (
			// mechanics FE (component 0 ... dim-1)
// 			dealii::FE_DGQArbitraryNodes<dim>
			dealii::FE_Q<dim>
				(dealii::QGaussLobatto<1> (p + 1)),
			1,
			// flux FE (component dim ... 2*dim-1)
			dealii::FE_RaviartThomas<dim>
				(p - 1),
			1
		);
		
		dof.distribute_dofs(*fe);
		dealii::DoFRenumbering::component_wise(dof);
		
		if (!MyPID)
			std::cout
				<< "dof.get_fe_collection().hp_constraints_are_implemented () = "
				<< dof.get_fe_collection().hp_constraints_are_implemented ()
				<< std::endl;
		MPI_Barrier(mpi_comm);
		
		ConstraintMatrix constraints;
		IndexSet relevant_set;
		DoFTools::extract_locally_relevant_dofs (dof, relevant_set);

		constraints.reinit(relevant_set);
		DoFTools::make_hanging_node_constraints(dof, constraints);
		
		if (!MyPID)
			constraints.print(std::cout);
		MPI_Barrier(mpi_comm);
		
		dof.clear();
	}
	catch (std::exception &exc) {
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
						<< std::endl;
			
			std::cerr	<< exc.what() << std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		return 1;
	}
	catch (...) {
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An UNKNOWN EXCEPTION occured!"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl << std::endl
						<< "Further information:" << std::endl
						<< "\tThe main() function catched an exception"
						<< std::endl
						<< "\twhich is not inherited from std::exception."
						<< std::endl
						<< "\tYou have probably called 'throw' somewhere,"
						<< std::endl
						<< "\tif you do not have done this, please contact the authors!"
						<< std::endl << std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		return 1;
	}
	
	return 0;
}
